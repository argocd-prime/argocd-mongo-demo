#!/usr/bin/env bash
# shellcheck disable=SC2164
set -exv
export BUILD_STREAM="${CI_COMMIT_REF_NAME//\//_}"
###########sshpass substituted with ssh private key login
echo "Current pipeline $CI_PIPELINE_IID is running as $BUILD_USER_ID user"
ssh-keyscan "$VM_IPADDRESS" >> ~/.ssh/known_hosts
eval "$(ssh-agent -s)"
echo "${SSH_PRIVATE_KEY}" | base64 -d | tr -d '\r' | ssh-add -
ssh -o StrictHostKeyChecking=no  $SSH_USER@$VM_IPADDRESS <<ENDSSH
